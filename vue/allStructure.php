<!DOCTYPE html>
<html lang="fr-FR">

<!-- Auteur : Jean-Brice GALLAND | Alain NIZARD | Mario DOS SANTOS -->
<!-- Modification : Mario/Jeremie B/Vincent/Charlotte/Cindy/Cyril/Nicolas/François/Virginie -->

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">

    <title>Document</title>

</head>

<body>

    <!-- Menu fixé en haut -->
    <div class="row fixed-top">
        <!-- Bouton retour -->
        <a href="switch.php?allPartner=set" class="btn btn-secondary col-3 pb-2" style="border-radius: 0 !important;">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-left-circle" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
            </svg>
        </a>
        <!-- Bouton vers liste partenaire -->
        <a href="switch.php?allPartner=set" class="btn btn-dark col-6 pb-2" style="border-radius: 0 !important;">Listes de structures</a>
        <!-- Bouton ajout -->
        <a href="switch.php?form=set&client_id=<?php echo $_GET['client_id'] ?>" class="btn btn-success col-3 pb-2" style="border-radius: 0 !important;">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
            </svg>
        </a>
    </div>

    <div id="app" class="container mt-5 px-0">
        <!-- Partenaire sélectionné -->
        <div class="row p-0">
            <div class="col d-flex g-0 border border-dark p-0 flex-wrap">
                <div class="m-3">
                    <img :src="partenaire.logo_url" class="rounded mx-auto d-block">
                </div>

                <div class="col align-self-center">
                    <h4>{{ partenaire.client_name }}</h5>
                        <h5 class="text-muted">ID : {{ partenaire.client_id }}</h5>
                        <div class="mt-3">
                            <a class="btn btn-success" :href="'switch.php?infoPartner=set&client_id='+partenaire.client_id" role="button" aria-expanded="false" aria-controls="collapseInfos">INFOS</a>
                        </div>
                </div>
            </div>
        </div>

        <?php
        echo $err;
        ?>

        <!-- Filtre -->
        <div class="row mt-4 mb-2 d-flex flex-column">
            <form action="" class="d-flex" method="GET">
                <div class="flex-grow-1 form-check">
                    <input class="form-check-input col" type="text" name="search" placeholder="ex: 1-23">
                </div>

                <div class="form-check form-check-inline transglo ml-2 col-2">
                    <input class="form-check-input" type="hidden" name="client_id" :value="partenaire.client_id">
                    <input class="form-check-input" type="hidden" name="allStructure" value="set">
                    <input class="form-check-button filtre" type="submit" value="Rechercher">
                </div>
            </form>

            <form action="" class="col mt-3" method="GET">
                <div class="form-check form-check-inline transglo">
                    <input class="form-check-input" type="radio" id="all" name="filter" value="all" checked>
                    <label class="form-check-label" for="all">Toutes</label>
                </div>

                <div class="form-check form-check-inline transglo">
                    <input class="form-check-input" type="radio" id="active" name="filter" value="active">
                    <label class="form-check-label" for="active">Actives</label>
                </div>

                <div class="form-check form-check-inline transglo">
                    <input class="form-check-input" type="radio" id="inactive" name="filter" value="inactive">
                    <label class="form-check-label" for="inactive">Inactives</label>
                </div>

                <div class="form-check form-check-inline transglo">
                    <input class="form-check-input" type="hidden" name="client_id" :value="partenaire.client_id">
                    <input class="form-check-input" type="hidden" name="allStructure" value="set">
                    <input class="form-check-button filtre" type="submit" value="Filtrer">
                </div>
            </form>
        </div>

        <!-- Structures -->
        <div class="row d-flex justify-content-between">
            <div v-for="struct in structures" class="col-md-6 border border-dark my-2 p-0 d-flex flex-wrap">
                <div class="m-3 align-self-center">
                    <img :src="struct.logo_url" class="rounded mx-auto d-block">
                </div>

                <div class="col align-self-center">
                    <h5>Structure N°{{ struct.branch_id }} de l'installation N°{{ struct.install_id }}</h5>
                    <form :id="struct.branch_id + 'droit_s' + struct.install_id" method="POST" :action="'switch.php?allStructure=set&client_id='+partenaire.client_id">
                        <div class="custom-control custom-switch" v-if="struct.active == 'Y'">
                            <input v-on:click="confirmation('desactiver',$event, struct.install_id, struct.branch_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+struct.branch_id+struct.install_id" checked>
                            <input type="hidden" name="active_struct" value="N">
                            <input type="hidden" name="branch_id" :value="struct.branch_id">
                            <input type="hidden" name="install_id" :value="struct.install_id">
                            <label class="custom-control-label" :for="'customSwitch'+struct.branch_id+struct.install_id">Active</label>
                        </div>

                        <div class="custom-control custom-switch" v-else>
                            <input v-on:click="confirmation('activer',$event, struct.install_id, struct.branch_id)" type="checkbox" class="custom-control-input" :id="'customSwitch'+struct.branch_id+struct.install_id">
                            <input type="hidden" name="active_struct" value="Y">
                            <input type="hidden" name="branch_id" :value="struct.branch_id">
                            <input type="hidden" name="install_id" :value="struct.install_id">
                            <label class="custom-control-label" :for="'customSwitch'+struct.branch_id+struct.install_id">Inactive</label>
                        </div>
                    </form>

                    <div class="mt-3">
                        <a class="btn btn-success btn-sm" data-toggle="collapse" :href="'#collapseGrants' + struct.install_id" role="button" aria-expanded="false" :aria-controls="'collapseGrants' + struct.install_id">Droits</a>
                    </div>
                </div>

                <div class="collapse col-12 px-3 border-dark" :id="'collapseGrants' + struct.install_id">
                    <form id="form_perms" method="POST" :action="'switch.php?allStructure=set&client_id='+partenaire.client_id">
                        <textarea name="input_perms" style="width:100%" rows="3">{{struct.perms}}</textarea>
                        <input type="hidden" name="branch_id" :value="struct.branch_id">
                        <input type="hidden" name="install_id" :value="struct.install_id">
                        <button type="button" v-on:click="confirmationg('modifier les droits')">envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Vue JS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <!-- JS -->
    <script>
        var donnees = <?php echo json_encode($data); ?>;

        var app = new Vue({
            el: "#app",
            data: function() {
                return donnees;
            },

            methods: {

                confirmation_p: function(message, event) {

                    result = confirm("etes vous sur de vouloir " + message);
                    if (result) {
                        document.forms["droit"].submit();
                    } else {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                },

                confirmation: function(message, event, install, branch) {
                    console.log(branch + install);
                    result = confirm("etes vous sur de vouloir " + message);
                    if (result) {
                        console.log(document.forms[branch + "droit_s" + install]);
                        document.forms[branch + "droit_s" + install].submit();
                    } else {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                },

                confirmationg: function(message) {
                    result = confirm("etes vous sur de vouloir " + message);
                    if (result) {
                        document.forms["form_perms"].submit();
                    } else {
                        document.location.reload();
                    }
                }
            }
        })
    </script>

</body>

</html>