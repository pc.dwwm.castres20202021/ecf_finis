<?php 

include_once('model/modelStructure.php');
include_once('model/modelePartner.php');

$clientId=$_GET['client_id'];

$data = MODELE_getPartnerclientid($clientId);

$active = "N";

if(isset($clientId) and isset($_POST['installId']) and isset($_POST['branchid'])) {
	MODELE_insertStructure($clientId, $_POST['installId'], $active, $data['default_perms'], $_POST['branchid']);
	$t = bin2hex(random_bytes(10));
	insertToken($_POST['installId'], $_POST['branchid'], $t);
	

	$mail = array ('id'=>$clientId,  'install'=>$_POST['installId'], 'branch'=>$_POST['branchid'], 'token'=>$t);
	// Redirection à faire vers le système de mail
	include('email/controleur/index.php');
} else {
	include('vue/form.php');
}
?>